import datetime
import gender_guesser.detector as gender

def oblicz_wiek(rok_urodzenia):
    obecny_rok = datetime.datetime.now().year
    return obecny_rok - rok_urodzenia

def rozroznij_imie(imie):
    zenskie_imiona = [
        'Anna', 'Maria', 'Katarzyna', 'Magdalena', 'Agnieszka', 'Ewa', 'Małgorzata', 'Zofia', 'Joanna', 'Aleksandra',
        'Barbara', 'Elżbieta', 'Jadwiga', 'Danuta', 'Irena', 'Teresa', 'Monika', 'Sylwia', 'Weronika', 'Gabriela',
        'Karolina', 'Julia', 'Natalia', 'Patrycja', 'Paulina', 'Martyna', 'Kinga', 'Justyna', 'Renata', 'Beata',
        'Alicja', 'Dorota', 'Halina', 'Izabela', 'Jolanta', 'Klaudia', 'Urszula', 'Wanda', 'Amelia', 'Aurelia',
        'Bogumiła', 'Cecylia', 'Daria', 'Edyta', 'Emilia', 'Genowefa', 'Grażyna', 'Helena', 'Inga', 'Jagna',
        'Janina', 'Kamila', 'Lidia', 'Ludmiła', 'Magda', 'Milena', 'Nina', 'Olga', 'Róża', 'Sabina',
        'Stefania', 'Tatiana', 'Ula', 'Waleria', 'Zuzanna', 'Żaneta', 'Agata', 'Anita', 'Ariadna', 'Blanka',
        'Bogusława', 'Brygida', 'Celina', 'Diana', 'Donata', 'Eleonora', 'Felicja', 'Franciszka', 'Gertruda', 'Hanna',
        'Honorata', 'Iga', 'Iwona', 'Jagoda', 'Jowita', 'Kalina', 'Leokadia', 'Liliana', 'Łucja', 'Malwina',
        'Michalina', 'Natasza', 'Oliwia', 'Radosława', 'Rozalia', 'Sonia', 'Teodora', 'Wiktoria', 'Zyta', 'Żaklina',
        'Adriana', 'Alina', 'Apolonia', 'Bernadeta', 'Bożena', 'Czesława', 'Dagmara', 'Emanuela', 'Ewelina',
        'Florentyna', 'Gosia', 'Hieronima', 'Ilona', 'Irmina', 'Judyta', 'Karina', 'Kornelia', 'Leonia', 'Lilianna',
        'Lucyna', 'Marianna', 'Michalina', 'Nadia', 'Oktawia', 'Przemysława', 'Regina', 'Roksana', 'Salomea',
        'Sławomira', 'Teodora', 'Ursula', 'Wiola', 'Zenobia', 'Żywia', 'Adrianna', 'Agnieszka', 'Albertina', 'Aneta',
        'Antonia', 'Balbina', 'Beniamina', 'Berta', 'Bibiana', 'Bolesława', 'Dagna', 'Eliza', 'Elwira', 'Fabiola',
        'Filomena', 'Gertruda', 'Henryka', 'Hermina', 'Ida', 'Jagienka', 'Jarosława', 'Kamelia', 'Larysa', 'Longina',
        'Matylda', 'Miranda', 'Norberta', 'Pola', 'Romualda', 'Rozanna', 'Samuela', 'Sylwana', 'Tamara', 'Wiara',
        'Władysława', 'Zenona', 'Żywia', 'Aniela', 'Antonina', 'Bronisława', 'Cecylia', 'Elwira', 'Faustyna', 'Gaja',
        'Irena', 'Izyda', 'Larysa', 'Łucja', 'Mira', 'Natalia', 'Ofelia', 'Pelagia', 'Rozanna', 'Stanisława', 'Tytusa',
        'Wacława'
    ]
    meskie_imiona = [
        'Jan', 'Andrzej', 'Piotr', 'Tomasz', 'Paweł', 'Krzysztof', 'Marek', 'Stanisław', 'Michał', 'Marcin',
        'Jerzy', 'Tadeusz', 'Adam', 'Zbigniew', 'Ryszard', 'Dariusz', 'Kazimierz', 'Henryk', 'Mariusz', 'Wojciech',
        'Robert', 'Mateusz', 'Łukasz', 'Artur', 'Jakub', 'Władysław', 'Józef', 'Grzegorz', 'Rafał', 'Karol',
        'Jacek', 'Radosław', 'Sebastian', 'Sławomir', 'Janusz', 'Antoni', 'Czesław', 'Damian', 'Emil', 'Filip',
        'Gabriel', 'Hubert', 'Ignacy', 'Jarosław', 'Kazimierz', 'Leon', 'Marian', 'Norbert', 'Oskar', 'Patryk',
        'Roman', 'Sylwester', 'Teodor', 'Wiktor', 'Zdzisław', 'Bartosz', 'Bruno', 'Cezary', 'Daniel', 'Edward',
        'Eugeniusz', 'Fryderyk', 'Gustaw', 'Herbert', 'Ireneusz', 'Juliusz', 'Konrad', 'Leszek', 'Miłosz', 'Natan',
        'Olgierd', 'Przemysław', 'Ryszard', 'Szymon', 'Tymoteusz', 'Urban', 'Wincenty', 'Zenon', 'Zygmunt', 'Bogdan',
        'Bożydar', 'Cyprian', 'Dominik', 'Ernest', 'Franciszek', 'Gerard', 'Hieronim', 'Ignacy', 'Jakub', 'Kamil',
        'Leonard', 'Maciej', 'Nicolas', 'Olgierd', 'Patrycjusz', 'Radosław', 'Sławomir', 'Tymon', 'Wacław', 'Zbigniew',
        'Adrian', 'Aleksander', 'Beniamin', 'Borys', 'Cezary', 'Daniel', 'Edward', 'Fabian', 'Ferdynand', 'Gustaw',
        'Herbert', 'Ignacy', 'Julian', 'Kornel', 'Leon', 'Marcel', 'Natan', 'Oleg', 'Pankracy', 'Roch',
        'Stefan', 'Teofil', 'Władysław', 'Zachariasz', 'Bartosz', 'Bogusław', 'Czesław', 'Damazy', 'Dionizy', 'Emilian',
        'Fryderyk', 'Gniewomir', 'Heronim', 'Ilian', 'Jaromir', 'Kordian', 'Lech', 'Melchior', 'Niegosław', 'Olgierd',
        'Prokop', 'Radomir', 'Sambor', 'Tytus', 'Wiesław', 'Ziemowit', 'Arnold', 'Bernard', 'Czesław', 'Damian',
        'Erwin', 'Felicjan', 'Ginter', 'Heronim', 'Jacek', 'Kajetan', 'Lesław', 'Maurycy', 'Norbert', 'Olgierd',
        'Roland', 'Sylwin', 'Teofil', 'Wawrzyniec', 'Ziemowit', 'Bogumił', 'Bonifacy', 'Cyprian', 'Dominik', 'Eliasz',
        'Feliks', 'Gracjan', 'Hilary', 'Inocenty', 'Józef', 'Krzysztof', 'Leon', 'Marceli', 'Nestor', 'Odon',
        'Prosimir', 'Rafał', 'Sambor', 'Tadeusz', 'Witold', 'Ziemowit'
    ]
    if imie in zenskie_imiona:
        return "żeńskie"
    elif imie in meskie_imiona:
        return "męskie"
    else:
        return "nieznane"

def generuj_kartke(imie_odbiorcy, wiek, wiadomosc, imie_nadawcy, plec_odbiorcy):
    forma_grzecznosciowa = "Drogi" if plec_odbiorcy == "męskie" else "Droga"
    kartka = f"""
    ---------------------------------------------
    |                                           |
    |  {forma_grzecznosciowa} {imie_odbiorcy},  |
    |                                           |
    |  Wszystkiego najlepszego z okazji         |
    |  Twoich {wiek} urodzin!                   |
    |                                           |
    |  {wiadomosc}                              |
    |                                           |
    |  Z pozdrowieniami,                        |
    |  {imie_nadawcy}  :)                       |
    |                                           |
    ---------------------------------------------
    """
    return kartka

# Pobieranie informacji
imie_odbiorcy = input("Podaj imię odbiorcy życzeń: ")
plec1 = rozroznij_imie(imie_odbiorcy)
print(f"Imię {imie_odbiorcy} jest {plec1}.") # jaka płeć
rok_urodzenia = int(input("Podaj rok urodzenia odbiorcy: "))
wiadomosc = input("Podaj spersonalizowaną wiadomość: ")
imie_nadawcy = input("Podaj imię nadawcy: ")

# Obliczanie wieku odbiorcy
wiek = oblicz_wiek(rok_urodzenia)

# Generowanie i wyświetlanie kartki urodzinowej
kartka = generuj_kartke(imie_odbiorcy, wiek, wiadomosc, imie_nadawcy, plec1)
print(kartka)
